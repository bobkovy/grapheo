# Grapheo

### What's a [Grapheo][]?

The Grapheo is a simple library for working with graphs.
 
The following types of graphs are currently supported:
- unweighted undirected graph
- unweighted directed graph
- weighted undirected graph
- weighted directed graph

The following operations with graphs are currently supported:
- add vertex
- add edge
- graph traversal
- find path between vertexes

## Getting started

### Installation

Add the following to your `pom.xml`'s dependencies section:

```xml
<dependency>
    <groupId>ru.nnov.graph-algorithms</groupId>
    <artifactId>grapheo</artifactId>
    <version>1.0-SNAPSHOT</version>
</dependency>
```
Note:  The library will not be published ;)
### Usage

#### Writing a new JobService

Usage with unweighted graph:

```java
import ru.nnov.grapheo.GraphBuilder;
import ru.nnov.grapheo.algorithms.pathfinder.impl.BreadthFirstSearch;
import ru.nnov.grapheo.algorithms.traverse.impl.BreadthFirstSearchTraverse;
import ru.nnov.grapheo.model.Graph;

import java.util.List;

public class Application {

    public static void main (String ... args) {
        Graph<Integer, Integer> graph = GraphBuilder.<Integer, Integer>builder()
                .withPathfinder(new BreadthFirstSearch<>())
                .withTraverser(new BreadthFirstSearchTraverse<>())
                .withWeightOnEdge(false)
                .withDirection(false)
                .addVertex(1)
                .addVertex(2)
                .addVertex(3)
                .addEdge(1, 2)
                .addEdge(1, 3)
                .addEdge(2, 3)
                .build();
        List<Integer> path = graph.getPath(1, 2);
        System.out.println(path); // [1, 2]

        graph.traverse(System.out::println);
        // 1
        // 2
        // 3
    }
}
```
Usage with weighted graph:
```java
import ru.nnov.grapheo.GraphBuilder;
import ru.nnov.grapheo.algorithms.pathfinder.impl.Dijkstra;
import ru.nnov.grapheo.algorithms.traverse.impl.BreadthFirstSearchTraverse;
import ru.nnov.grapheo.model.Graph;

import java.util.List;

public class Application {

    public static void main (String ... args) {
        Graph<Integer, Integer> graph = GraphBuilder.<Integer, Integer>builder()
                .withPathfinder(new Dijkstra<>())
                .withTraverser(new BreadthFirstSearchTraverse<>())
                .withWeightOnEdge(true)
                .withDirection(true)
                .addVertex(1)
                .addVertex(2)
                .addVertex(3)
                .addEdge(1, 2, 1)
                .addEdge(1, 3, 10)
                .addEdge(2, 3, 5)
                .build();
        List<Integer> path = graph.getPath(1, 3);
        System.out.println(path); // [1, 2, 3] 

        graph.traverse(System.out::println);
        // 1
        // 2
        // 3
    }
}
```
#### Graph
```java
package ru.nnov.grapheo.model;

import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public interface Graph<T, W extends Number> {
    /**
     * Add vertex to the graph
     *
     * @param vertex to add
     */
    void addVertex(T vertex);

    /**
     * Get all graph vertices
     *
     * @return grap vertices
     */
    Set<T> getVertexSet();

    /**
     * Method finds path between {fromVertex} and {toVertex} vertices.
     * This call will be delegated to the @{@link ru.nnov.grapheo.algorithms.pathfinder.Pathfinder}
     * which must be passed to the graph constructor
     *
     * @param fromVertex the beginning of the path
     * @param toVertex   end of path
     * @return ordered list with vertices
     * @throws IllegalStateException if no path found
     */
    List<T> getPath(T fromVertex, T toVertex);

    /**
     * The method checks if the graph contains a vertex or not
     *
     * @param vertex for check
     * @return true - if contains, false - otherwise
     */
    boolean contains(T vertex);

    /**
     * Add edge with weight to the graph. Loops are ignored.
     *
     * @param fromVertex initial vertex of edge
     * @param toVertex   end vertex
     * @param weight     of the edge. Will be ignored if the graph is not weighted
     * @throws IllegalArgumentException if graph does'not contains fromVertex or toVertex vertexes
     * @throws IllegalArgumentException if graph is weighted weight is null
     */
    void addEdge(T fromVertex, T toVertex, W weight);

    /**
     * Add edge with weight to the graph. Loops are ignored.
     *
     * @param fromVertex initial vertex of edge
     * @param toVertex   end vertex
     * @throws IllegalArgumentException if graph does'not contains fromVertex or toVertex vertexes
     * @throws IllegalArgumentException if graph is weighted
     */
    void addEdge(T fromVertex, T toVertex);

    /**
     * Visiting each vertex in a graph and call consumer on each vertex.
     * This call will be delegated to the @{@link ru.nnov.grapheo.algorithms.traverse.Traverse}
     * which must be passed to the graph constructor
     * @param consumer will be called on each vertex
     * @throws IllegalArgumentException if consumer is null
     */
    void traverse(Consumer<T> consumer);

    /**
     * Method returns an adjacency map without weight
     * @return adjacency map, there key is a vertex and a value is a set of adjacent vertices with the key
     */
    Map<T, Set<T>> getAdjacencyMap();

    /**
     * Method returns an adjacency map with weight to each vertex
     * @return adjacency map, there key is a vertex and a value is a set of adjacent vertices with the key
     */
    Map<T, Set<Pair<T, W>>> getAdjacencyMapWithWeight();

    /**
     * A graph whose vertices and edges have not been assigned weights; the opposite of a weighted graph.
     *
     * @return true - if graph is weighted, false - otherwise
     */
    boolean isWeighted();

    /**
     * A directed graph is one in which the edges have a distinguished direction, from one vertex to another.
     * @return true - if graph is directed, false - otherwise
     */
    boolean isDirected();
}

```

#### Adjacency Storage
```java
package ru.nnov.grapheo.model;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;

public interface AdjacencyStorage<T, W extends Number> {
    /**
     * Add edge with weight.
     *
     * @param fromVertex initial vertex of edge
     * @param toVertex   end vertex
     * @param weight     of the edge. Will be ignored if the graph is not weighted
     * @throws IllegalArgumentException if graph does'not contains fromVertex or toVertex vertexes
     * @throws IllegalArgumentException if graph is weighted weight is null
     */
    void addToAdjacencyMap(T fromVertex, T toVertex, W weight);

    /**
     * Add edge without weight.
     * 
     * @param fromVertex initial vertex of edge
     * @param toVertex   end vertex
     * @throws IllegalArgumentException if graph does'not contains fromVertex or toVertex vertexes
     * @throws IllegalArgumentException if graph is weighted
     */
    void addToAdjacencyMap(T fromVertex, T toVertex);

    /**
     * Method returns an adjacency map with weight to each vertex
     *
     * @return adjacency map, there key is a vertex and a value is a set of adjacent vertices with the key
     */
    Map<T, Set<Pair<T, W>>> getAdjacencyMapWithWeight();

    /**
     * Method returns an adjacency map without weight
     *
     * @return adjacency map, there key is a vertex and a value is a set of adjacent vertices with the key
     */
    Map<T, Set<T>> getAdjacencyMap();

    /**
     * A graph whose vertices and edges have not been assigned weights; the opposite of a weighted graph.
     *
     * @return true - if graph is weighted, false - otherwise
     */
    boolean isWeighted();
}

```

#### Pathfinder

```java
package ru.nnov.grapheo.algorithms.pathfinder;

import ru.nnov.grapheo.model.Graph;

import java.util.List;

/**
 * Finds a path between two vertices in the graph
 * @param <T> vertex type
 */
public interface Pathfinder<T> {
    /**
     * Is Pathfinder applicable to the graph or not?
     * @param graph do you support graph or not
     * @return true if supported, false - otherwise
     */
    boolean isSupported(Graph graph);

    /**
     * Method finds path between {fromVertex} and {toVertex} vertices
     * @param graph search space
     * @param fromVertex the beginning of the path
     * @param toVertex end of path
     * @return ordered list with vertices
     * @throws IllegalStateException if no path found
     */
    List<T> findPath(Graph<T, ? extends Number> graph, T fromVertex, T toVertex);
}
```
The library provides the following Pathfinders:

- **ru.nnov.grapheo.algorithms.pathfinder.impl.BreadthFirstSearch** - works with unweighted graph
- **ru.nnov.grapheo.algorithms.pathfinder.impl.Dijkstra** - only works with a weighted graph with a positive weight on the edges

####Traverse
```java
package ru.nnov.grapheo.algorithms.traverse;

import ru.nnov.grapheo.model.Graph;

import java.util.function.Consumer;

/**
 * Visiting each vertex in a graph and call consumer on each vertex.
 * @param <T> type of vertex
 */
public interface Traverse<T> {
    /**
     * Visiting each vertex in a graph and call consumer on each vertex.
     * @param graph for  traversal
     * @param consumer will be called on each vertex
     */
    void traverse(Graph<T, ? extends Number> graph, Consumer<T> consumer);
}

```

The library provides the following Traverses:
- *ru.nnov.grapheo.algorithms.traverse.impl.BreadthFirstSearchTraverse*


[Grapheo]: https://bitbucket.org/bobkovy/grapheo/src/master/
