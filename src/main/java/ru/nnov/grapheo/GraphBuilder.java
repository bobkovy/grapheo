package ru.nnov.grapheo;

import ru.nnov.grapheo.algorithms.pathfinder.Pathfinder;
import ru.nnov.grapheo.algorithms.traverse.Traverse;
import ru.nnov.grapheo.model.AdjacencyStorage;
import ru.nnov.grapheo.model.Graph;
import ru.nnov.grapheo.model.impl.SimpleAdjacencyStorage;
import ru.nnov.grapheo.model.impl.SimpleGraph;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GraphBuilder<T, W extends Number> {

    private Pathfinder<T> pathfinder;
    private AdjacencyStorage<T, W> adjacencyStorage;
    private Traverse<T> traverser;
    private Boolean isDirected;
    private Boolean isWeighted;
    private final Set<T> vertexes = new HashSet<>();
    private final Set<Edge> edges = new HashSet<>();

    private GraphBuilder() {

    }

    public static <T, W extends Number> GraphBuilder<T, W> builder() {
        return new GraphBuilder<>();
    }

    public GraphBuilder<T, W> withPathfinder(Pathfinder<T> pathfinder) {
        this.pathfinder = pathfinder;
        return this;
    }

    public GraphBuilder<T, W> addVertex(T vertex) {
        vertexes.add(vertex);
        return this;
    }

    public GraphBuilder<T, W> addEdge(T fromVertex, T toVertex, W weight) {
        edges.add(new Edge(fromVertex, toVertex, weight));
        return this;
    }

    public GraphBuilder<T, W> addEdge(T fromVertex, T toVertex) {
        return addEdge(fromVertex, toVertex, null);
    }

    public GraphBuilder<T, W> withWeightOnEdge(boolean isWeighted) {
        this.isWeighted = isWeighted;
        return this;
    }

    public GraphBuilder<T, W> withTraverser(Traverse<T> traverser) {
        this.traverser = traverser;
        return this;
    }

    public GraphBuilder<T, W> withDirection(boolean isDirected) {
        this.isDirected = isDirected;
        return this;
    }

    @SuppressWarnings("WeakerAccess")
    public GraphBuilder<T, W> withAdjacencyStorage(AdjacencyStorage<T, W> adjacencyStorage) {
        this.adjacencyStorage = adjacencyStorage;
        return this;
    }

    public Graph<T, W> build() {
        if (traverser == null) {
            traverser = (graph, consumer) -> {
                throw new UnsupportedOperationException("Graph contains empty traverser");
            };
        }
        if (isWeighted == null && adjacencyStorage == null) {
            throw new IllegalArgumentException(
                    "Do you need a weighted graph? If yes, use withWeightOnEdge (true), otherwise withWeightOnEdge (false)");
        }
        if (isDirected == null) {
            throw new IllegalArgumentException(
                    "Do you need a directed graph? If yes, use withDirection (true), otherwise withDirection (false)");
        }
        if (adjacencyStorage == null) {
            adjacencyStorage = new SimpleAdjacencyStorage<>(isWeighted);
        }

        if (pathfinder == null) {
            pathfinder = createEmptyPathfinder();
        }

        Graph<T, W> graph = new SimpleGraph<>(pathfinder, adjacencyStorage, traverser, isDirected);
        vertexes.forEach(graph::addVertex);
        edges.forEach(edge -> graph.addEdge(edge.fromVertex, edge.toVertex, edge.weight));
        return graph;
    }

    private Pathfinder<T> createEmptyPathfinder() {
        return new Pathfinder<T>() {
            @Override
            public boolean isSupported(Graph graph) {
                return true;
            }

            @Override
            public List<T> findPath(Graph<T, ? extends Number> graph, T fromVertex, T toVertex) {
                throw new UnsupportedOperationException("Graph contains empty pathfinder");
            }
        };
    }

    private class Edge {
        final T fromVertex;
        final T toVertex;
        final W weight;

        private Edge(T fromVertex, T toVertex, W weight) {
            this.fromVertex = fromVertex;
            this.toVertex = toVertex;
            this.weight = weight;
        }
    }
}
