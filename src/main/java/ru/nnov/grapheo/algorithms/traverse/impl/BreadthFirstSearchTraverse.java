package ru.nnov.grapheo.algorithms.traverse.impl;

import ru.nnov.grapheo.algorithms.traverse.Traverse;
import ru.nnov.grapheo.model.Graph;

import java.util.*;
import java.util.function.Consumer;

import static java.util.Collections.emptySet;

public class BreadthFirstSearchTraverse<T> implements Traverse<T> {
    @Override
    public void traverse(Graph<T, ? extends Number> graph, Consumer<T> consumer) {
        Queue<T> queue = new LinkedList<>();
        Set<T> visited = new HashSet<>();
        Map<T, Set<T>> adjacencyMap = graph.getAdjacencyMap();
        findUnvisitedVertex(graph, visited, queue::add);
        while (queue.size() != 0) {
            T parentVertex = queue.poll();
            if (!visited.contains(parentVertex)) {
                visitVertex(parentVertex, consumer, queue, visited);
            }
            adjacencyMap.getOrDefault(parentVertex, emptySet())
                    .stream()
                    .filter(vertex -> !visited.contains(vertex))
                    .forEach(vertex -> visitVertex(vertex, consumer, queue, visited));
            if (queue.isEmpty()) {
                findUnvisitedVertex(graph, visited, queue::add);
            }
        }
    }

    private void visitVertex(T parentVertex, Consumer<T> consumer, Queue<T> queue, Set<T> visited) {
        visited.add(parentVertex);
        queue.add(parentVertex);
        consumer.accept(parentVertex);
    }

    private void findUnvisitedVertex(Graph<T, ? extends Number> graph, Set<T> visited, Consumer<T> vertexConsumer) {
        graph.getVertexSet().stream()
                .filter(vertex -> !visited.contains(vertex))
                .findFirst()
                .ifPresent(vertexConsumer);
    }
}
