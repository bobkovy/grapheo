package ru.nnov.grapheo.algorithms.traverse;

import ru.nnov.grapheo.model.Graph;

import java.util.function.Consumer;

/**
 * Visiting each vertex in a graph and call consumer on each vertex.
 *
 * @param <T> type of vertex
 */
public interface Traverse<T> {
    /**
     * Visiting each vertex in a graph and call consumer on each vertex.
     * @param graph for  traversal
     * @param consumer will be called on each vertex
     */
    void traverse(Graph<T, ? extends Number> graph, Consumer<T> consumer);
}
