package ru.nnov.grapheo.algorithms.pathfinder.impl;

import ru.nnov.grapheo.algorithms.pathfinder.Pathfinder;
import ru.nnov.grapheo.model.Graph;

import java.util.*;

import static java.util.Collections.emptySet;

public class BreadthFirstSearch<T> implements Pathfinder<T> {

    @Override
    public boolean isSupported(Graph graph) {
        return !graph.isWeighted();
    }

    public List<T> findPath(Graph<T, ? extends Number> graph, T fromVertex, T toVertex) {
        Map<T, T> reversedPath = searchPath(graph.getAdjacencyMap(), fromVertex);
        if (!reversedPath.containsKey(toVertex)) {
            throw new IllegalStateException("No path found from " + fromVertex + " to " + toVertex);
        }

        List<T> path = new LinkedList<>();
        T pathVertex = toVertex;
        while (pathVertex != null) {
            path.add(0, pathVertex);
            if (pathVertex.equals(fromVertex)) {
                break;
            }
            pathVertex = reversedPath.get(pathVertex);
        }
        return path;
    }

    private Map<T, T> searchPath(Map<T, Set<T>> adjacencyMap, T fromVertex) {
        Queue<T> queue = new LinkedList<>();
        Set<T> visited = new HashSet<>();
        Map<T, T> reversedPath = new HashMap<>();
        // Mark the current node as visited and enqueue it
        queue.add(fromVertex);

        while (queue.size() != 0) {
            T parentVertex = queue.poll();
            adjacencyMap.getOrDefault(parentVertex, emptySet())
                    .stream()
                    .filter(vertex -> !visited.contains(vertex))
                    .forEach(vertex -> {
                        reversedPath.put(vertex, parentVertex);
                        visited.add(vertex);
                        queue.add(vertex);
                    });
        }

        return reversedPath;
    }
}
