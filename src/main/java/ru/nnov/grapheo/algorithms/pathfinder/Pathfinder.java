package ru.nnov.grapheo.algorithms.pathfinder;

import ru.nnov.grapheo.model.Graph;

import java.util.List;

/**
 * Finds a path between two vertices in the graph
 *
 * @param <T> vertex type
 */
public interface Pathfinder<T> {
    /**
     * Is Pathfinder applicable to the graph or not?
     * @param graph do you support graph or not
     * @return true if supported, false - otherwise
     */
    boolean isSupported(Graph graph);

    /**
     * Method finds path between {fromVertex} and {toVertex} vertices
     * @param graph search space
     * @param fromVertex the beginning of the path
     * @param toVertex end of path
     * @return ordered list with vertices
     * @throws IllegalStateException if no path found
     */
    List<T> findPath(Graph<T, ? extends Number> graph, T fromVertex, T toVertex);
}
