package ru.nnov.grapheo.algorithms.pathfinder.impl;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import ru.nnov.grapheo.algorithms.pathfinder.Pathfinder;
import ru.nnov.grapheo.model.Graph;

import java.util.*;

import static java.util.Collections.emptySet;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

public class Dijkstra<T> implements Pathfinder<T> {
    @Override
    public boolean isSupported(Graph graph) {
        return graph.isWeighted();
    }

    @Override
    public List<T> findPath(Graph<T, ? extends Number> graph, T fromVertex, T toVertex) {
        Map<T, WrappedVertex<T>> wrappedVertex = wrapVertexes(graph);
        Map<T, Set<Pair<WrappedVertex<T>, Double>>> adjustedMap = wrapVertexesInAdjustedMap(graph, wrappedVertex);

        Set<WrappedVertex<T>> visited = new HashSet<>();
        Queue<WrappedVertex<T>> unvisitedQueue = new LinkedList<>();

        WrappedVertex<T> from = new WrappedVertex<>(fromVertex);
        from.distance = 0d;
        unvisitedQueue.add(from);

        while (unvisitedQueue.size() != 0) {
            WrappedVertex<T> currentVertex = findVertexWithMinDistance(unvisitedQueue);
            unvisitedQueue.remove(currentVertex);

            adjustedMap.getOrDefault(currentVertex.vertex, emptySet())
                    .stream()
                    .filter(pairWithNodeAndWeight -> !visited.contains(pairWithNodeAndWeight.getKey()))
                    .forEach(neighborVertex -> {
                        updateShortestPathAndDistanceToOptimalValue(
                                currentVertex,
                                neighborVertex.getKey(),
                                neighborVertex.getValue());
                        unvisitedQueue.add(neighborVertex.getKey());
                    });
            visited.add(currentVertex);
        }
        List<T> path = wrappedVertex.get(toVertex).shortestPath;
        if (path == null || path.isEmpty()) {
            throw new IllegalStateException("No path from " + from + " to" + toVertex);
        }
        path.add(toVertex);
        return path;
    }

    private Map<T, Set<Pair<WrappedVertex<T>, Double>>> wrapVertexesInAdjustedMap(Graph<T, ? extends Number> graph,
                                                                                  Map<T, WrappedVertex<T>> wrappedVertex) {
        return graph.getAdjacencyMapWithWeight().entrySet()
                .stream()
                .map(entry -> new ImmutablePair<>(entry.getKey(), wrapAdjustedVertex(entry, wrappedVertex)))
                .collect(toMap(Pair::getKey, Pair::getValue));
    }

    private <W extends Number> Set<Pair<WrappedVertex<T>, Double>> wrapAdjustedVertex(
            Map.Entry<T, Set<Pair<T, W>>> entry,
            Map<T, WrappedVertex<T>> wrappedVertex) {
        return entry.getValue().stream()
                .map(pair -> new ImmutablePair<>(wrappedVertex.get(pair.getKey()), pair.getValue().doubleValue()))
                .collect(toSet());
    }

    private Map<T, WrappedVertex<T>> wrapVertexes(Graph<T, ? extends Number> graph) {
        return graph.getVertexSet().stream()
                .map(vertex -> new ImmutablePair<>(vertex, new WrappedVertex<>(vertex)))
                .collect(toMap(Pair::getKey, Pair::getValue));
    }

    private WrappedVertex<T> findVertexWithMinDistance(Collection<WrappedVertex<T>> unsettledNodes) {
        return unsettledNodes.stream()
                .min(Comparator.comparingDouble(o -> o.distance))
                .orElse(new WrappedVertex<>(null));
    }

    private void updateShortestPathAndDistanceToOptimalValue(
            WrappedVertex<T> currentVertex, WrappedVertex<T> neighborVertex, Double edgeWeigh) {
        Double sourceDistance = currentVertex.distance;
        if (edgeWeigh < 0d) {
            throw new IllegalArgumentException("Negative weight not supported");
        }
        if (sourceDistance + edgeWeigh < neighborVertex.distance) {
            neighborVertex.distance = sourceDistance + edgeWeigh;
            neighborVertex.shortestPath = new ArrayList<>(currentVertex.shortestPath);
            neighborVertex.shortestPath.add(currentVertex.vertex);
        }
    }

    private class WrappedVertex<H> {
        private final H vertex;

        private List<H> shortestPath = new ArrayList<>();

        private Double distance = Double.MAX_VALUE;

        private WrappedVertex(H vertex) {
            this.vertex = vertex;
        }
    }
}
