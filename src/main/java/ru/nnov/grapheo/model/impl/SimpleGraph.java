package ru.nnov.grapheo.model.impl;

import org.apache.commons.lang3.tuple.Pair;
import ru.nnov.grapheo.algorithms.pathfinder.Pathfinder;
import ru.nnov.grapheo.algorithms.traverse.Traverse;
import ru.nnov.grapheo.model.AdjacencyStorage;
import ru.nnov.grapheo.model.Graph;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class SimpleGraph<T, W extends Number> implements Graph<T, W> {
    private final Pathfinder<T> pathfinder;
    private final AdjacencyStorage<T, W> adjacencyStorage;
    private final Traverse<T> traverser;
    private final Boolean isDirected;
    private final Set<T> vertexSet = new HashSet<>();

    public SimpleGraph(
            Pathfinder<T> pathfinder,
            AdjacencyStorage<T, W> adjacencyMap,
            Traverse<T> traverser,
            boolean isDirected) {

        if (pathfinder == null || adjacencyMap == null || traverser == null) {
            throw new IllegalArgumentException("Arguments must be nonnull");
        }
        this.pathfinder = pathfinder;
        this.adjacencyStorage = adjacencyMap;
        this.traverser = traverser;
        this.isDirected = isDirected;
        if (!pathfinder.isSupported(this)) {
            throw new IllegalArgumentException("Pathfinder doesn't support your graph");
        }
    }

    public synchronized void addVertex(T vertex) {
        if (vertex == null) {
            throw new IllegalArgumentException("Vertex must be nonnull");
        }
        vertexSet.add(vertex);
    }

    private void throwExceptionIfVertexNotFound(T fromVertex, T toVertex) {
        Stream.of(fromVertex, toVertex)
                .filter(vertex -> !contains(vertex))
                .findAny()
                .ifPresent(illegalVertex -> {
                    throw new IllegalArgumentException("Graph does not contains vertex: " + illegalVertex);
                });
    }

    public synchronized void addEdge(T fromVertex, T toVertex, W weight) {
        throwExceptionIfVertexNotFound(fromVertex, toVertex);
        if (fromVertex == toVertex) {
            return;
        }
        addToAdjacencyMap(fromVertex, toVertex, weight);
        if (!isDirected) {
            addToAdjacencyMap(toVertex, fromVertex, weight);
        }
    }

    @Override
    public void addEdge(T fromVertex, T toVertex) {
        addEdge(fromVertex, toVertex, null);
    }


    private void addToAdjacencyMap(T fromVertex, T toVertex, W weight) {
        adjacencyStorage.addToAdjacencyMap(fromVertex, toVertex, weight);
    }

    public synchronized List<T> getPath(T fromVertex, T toVertex) {
        return pathfinder.findPath(this, fromVertex, toVertex);
    }

    @Override
    public synchronized void traverse(Consumer<T> consumer) {
        if (consumer == null) {
            throw new IllegalArgumentException("Consumer must be non null");
        }
        traverser.traverse(this, consumer);
    }

    @Override
    public synchronized Map<T, Set<T>> getAdjacencyMap() {
        return new HashMap<>(adjacencyStorage.getAdjacencyMap());
    }

    @Override
    public synchronized Map<T, Set<Pair<T, W>>> getAdjacencyMapWithWeight() {
        return new HashMap<>(adjacencyStorage.getAdjacencyMapWithWeight());
    }

    @Override
    public boolean isWeighted() {
        return adjacencyStorage.isWeighted();
    }

    @Override
    public boolean isDirected() {
        return isDirected;
    }

    public boolean contains(T vertex) {
        return vertex != null && vertexSet.contains(vertex);
    }

    public Set<T> getVertexSet() {
        return vertexSet;
    }
}
