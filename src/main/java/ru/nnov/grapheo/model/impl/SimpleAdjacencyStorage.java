package ru.nnov.grapheo.model.impl;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import ru.nnov.grapheo.model.AdjacencyStorage;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static java.util.stream.Collectors.toMap;
import static java.util.stream.Collectors.toSet;

public class SimpleAdjacencyStorage<T, W extends Number> implements AdjacencyStorage<T, W> {
    private final Boolean isWeighted;
    private final Map<T, Set<Pair<T, W>>> adjacencyMap = new HashMap<>();

    public SimpleAdjacencyStorage(boolean isWeighted) {
        this.isWeighted = isWeighted;
    }

    @Override
    public void addToAdjacencyMap(T fromVertex, T toVertex, W weight) {
        if (isWeighted && weight == null) {
            throw new IllegalArgumentException("Weight must be non null");
        }
        adjacencyMap.computeIfAbsent(fromVertex, k -> new HashSet<>())
                .add(new ImmutablePair<>(toVertex, weight));
    }

    @Override
    public void addToAdjacencyMap(T fromVertex, T toVertex) {
        addToAdjacencyMap(fromVertex, toVertex, null);
    }

    @Override
    public Map<T, Set<Pair<T, W>>> getAdjacencyMapWithWeight() {
        return adjacencyMap;
    }

    @Override
    public Map<T, Set<T>> getAdjacencyMap() {
        return adjacencyMap.entrySet().stream()
                .map(entry -> new ImmutablePair<>(entry.getKey(), entry.getValue().stream()
                        .map(Pair::getKey)
                        .collect(toSet())))
                .collect(toMap(Pair::getKey, Pair::getValue));
    }

    @Override
    public boolean isWeighted() {
        return isWeighted;
    }
}
