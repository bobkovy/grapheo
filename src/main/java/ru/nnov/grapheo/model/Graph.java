package ru.nnov.grapheo.model;

import org.apache.commons.lang3.tuple.Pair;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;

public interface Graph<T, W extends Number> {
    /**
     * Add vertex to the graph
     *
     * @param vertex to add
     */
    void addVertex(T vertex);

    /**
     * Get all graph vertices
     *
     * @return graph vertices
     */
    Set<T> getVertexSet();

    /**
     * Method finds path between {fromVertex} and {toVertex} vertices.
     * This call will be delegated to the @{@link ru.nnov.grapheo.algorithms.pathfinder.Pathfinder}
     * which must be passed to the graph constructor
     *
     * @param fromVertex the beginning of the path
     * @param toVertex   end of path
     * @return ordered list with vertices
     * @throws IllegalStateException if no path found
     */
    List<T> getPath(T fromVertex, T toVertex);

    /**
     * The method checks if the graph contains a vertex or not
     *
     * @param vertex for check
     * @return true - if contains, false - otherwise
     */
    @SuppressWarnings("unused")
    boolean contains(T vertex);

    /**
     * Add edge with weight to the graph. Loops are ignored.
     * This call will be delegated to the @{@link ru.nnov.grapheo.model.AdjacencyStorage}
     *
     * @param fromVertex initial vertex of edge
     * @param toVertex   end vertex
     * @param weight     of the edge. Will be ignored if the graph is not weighted
     * @throws IllegalArgumentException if graph does'not contains fromVertex or toVertex vertexes
     * @throws IllegalArgumentException if graph is weighted weight is null
     */
    void addEdge(T fromVertex, T toVertex, W weight);

    /**
     * Add edge without weight to the graph. Loops are ignored.
     * This call will be delegated to the @{@link ru.nnov.grapheo.model.AdjacencyStorage}
     *
     * @param fromVertex initial vertex of edge
     * @param toVertex   end vertex
     * @throws IllegalArgumentException if graph does'not contains fromVertex or toVertex vertexes
     * @throws IllegalArgumentException if graph is weighted
     */
    void addEdge(T fromVertex, T toVertex);

    /**
     * Visiting each vertex in a graph and call consumer on each vertex.
     * This call will be delegated to the @{@link ru.nnov.grapheo.algorithms.traverse.Traverse}
     * which must be passed to the graph constructor
     *
     * @param consumer will be called on each vertex
     * @throws IllegalArgumentException if consumer is null
     */
    void traverse(Consumer<T> consumer);

    /**
     * Method returns an adjacency map without weight
     * This call will be delegated to the @{@link ru.nnov.grapheo.model.AdjacencyStorage}
     *
     * @return adjacency map, there key is a vertex and a value is a set of adjacent vertices with the key
     */
    Map<T, Set<T>> getAdjacencyMap();

    /**
     * Method returns an adjacency map with weight to each vertex
     * This call will be delegated to the @{@link ru.nnov.grapheo.model.AdjacencyStorage}
     *
     * @return adjacency map, there key is a vertex and a value is a set of adjacent vertices with the key
     */
    Map<T, Set<Pair<T, W>>> getAdjacencyMapWithWeight();

    /**
     * A graph whose vertices and edges have not been assigned weights; the opposite of a weighted graph.
     * This call will be delegated to the @{@link ru.nnov.grapheo.model.AdjacencyStorage}
     * @return true - if graph is weighted, false - otherwise
     */
    boolean isWeighted();

    /**
     * A directed graph is one in which the edges have a distinguished direction, from one vertex to another.
     * @return true - if graph is directed, false - otherwise
     */
    boolean isDirected();
}
