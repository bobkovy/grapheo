package ru.nnov.grapheo.model;

import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Set;

public interface AdjacencyStorage<T, W extends Number> {
    /**
     * Add edge with weight.
     *
     * @param fromVertex initial vertex of edge
     * @param toVertex   end vertex
     * @param weight     of the edge. Will be ignored if the graph is not weighted
     * @throws IllegalArgumentException if graph does'not contains fromVertex or toVertex vertexes
     * @throws IllegalArgumentException if graph is weighted weight is null
     */
    void addToAdjacencyMap(T fromVertex, T toVertex, W weight);

    /**
     * Add edge without weight.
     *
     * @param fromVertex initial vertex of edge
     * @param toVertex   end vertex
     * @throws IllegalArgumentException if graph does'not contains fromVertex or toVertex vertexes
     * @throws IllegalArgumentException if graph is weighted
     */
    void addToAdjacencyMap(T fromVertex, T toVertex);

    /**
     * Method returns an adjacency map with weight to each vertex
     *
     * @return adjacency map, there key is a vertex and a value is a set of adjacent vertices with the key
     */
    Map<T, Set<Pair<T, W>>> getAdjacencyMapWithWeight();

    /**
     * Method returns an adjacency map without weight
     *
     * @return adjacency map, there key is a vertex and a value is a set of adjacent vertices with the key
     */
    Map<T, Set<T>> getAdjacencyMap();

    /**
     * A graph whose vertices and edges have not been assigned weights; the opposite of a weighted graph.
     *
     * @return true - if graph is weighted, false - otherwise
     */
    boolean isWeighted();
}
