package ru.nnov.grapheo;

import org.junit.Before;
import org.junit.Test;
import ru.nnov.grapheo.algorithms.pathfinder.impl.BreadthFirstSearch;
import ru.nnov.grapheo.algorithms.traverse.impl.BreadthFirstSearchTraverse;
import ru.nnov.grapheo.model.Graph;
import ru.nnov.grapheo.model.impl.SimpleAdjacencyStorage;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GraphBuilderTest {

    private GraphBuilder<Integer, Integer> builder;

    @Before
    public void setUp() {
        /*
         *     (11)
         *   /
         * (0)-(1)-(2)-(3)-(4)-(5)-(6)-(7)-(8)-(9) (10)
         *  \__________________________________/
         */

        builder = GraphBuilder.builder();
        IntStream.range(0, 12).forEach(builder::addVertex);
        IntStream.range(0, 9).forEach(i -> builder.addEdge(i, i + 1));
        builder.addEdge(0, 9)
                .addEdge(0, 11);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void emptyPathFinder() {
        builder
                .withDirection(false)
                .withWeightOnEdge(false).build().getPath(1, 2);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void emptyTraverser() {
        builder.withDirection(false)
                .withWeightOnEdge(false).build().traverse(System.out::println);
    }

    @Test(expected = IllegalArgumentException.class)
    public void withUnsetDirection() {
        builder
                .withWeightOnEdge(false)
                .build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void withUnsetWeight() {
        builder
                .withDirection(false)
                .build();
    }

    @Test
    public void normalCase() {
        Graph<Integer, Integer> graph = builder.withDirection(false)
                .withAdjacencyStorage(new SimpleAdjacencyStorage<>(false))
                .withPathfinder(new BreadthFirstSearch<>())
                .withTraverser(new BreadthFirstSearchTraverse<>())
                .build();
        List<Integer> path = graph.getPath(6, 3);
        assertNotNull(path);
        assertEquals(Arrays.asList(6, 5, 4, 3), path);
        final int[] count = {0};
        graph.traverse(integer -> count[0]++);
        assertEquals(graph.getVertexSet().size(), count[0]);
    }
}