package ru.nnov.grapheo.model.impl;

import org.junit.Before;
import org.junit.Test;
import ru.nnov.grapheo.GraphBuilder;
import ru.nnov.grapheo.algorithms.pathfinder.impl.BreadthFirstSearch;
import ru.nnov.grapheo.algorithms.traverse.impl.BreadthFirstSearchTraverse;
import ru.nnov.grapheo.model.Graph;

import java.util.stream.IntStream;

public class GraphTest {
    private Graph<Integer, Integer> graph;

    @Before
    public void setUp() {
        /*
         *     (11)
         *   /
         * (0)-(1)-(2)-(3)-(4)-(5)-(6)-(7)-(8)-(9) (10)
         *  \__________________________________/
         */

        GraphBuilder<Integer, Integer> builder = GraphBuilder.<Integer, Integer>builder()
                .withDirection(false)
                .withWeightOnEdge(false);
        IntStream.range(0, 12).forEach(builder::addVertex);
        IntStream.range(0, 9).forEach(i -> builder.addEdge(i, i + 1));
        builder.addEdge(0, 9)
                .addEdge(0, 11);
        graph = builder.build();
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyPathFinder() {
        new SimpleGraph<>(null,
                new SimpleAdjacencyStorage<Integer, Integer>(false),
                new BreadthFirstSearchTraverse<>(),
                false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void emptyGraphType() {
        new SimpleGraph<>(new BreadthFirstSearch<>(), null, new BreadthFirstSearchTraverse<>(), false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void unknownVertex() {
        graph.addEdge(11, Integer.MAX_VALUE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullVertex() {
        graph.addVertex(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void incorrectPathFinder() {
        new SimpleGraph<>(new BreadthFirstSearch<>(),
                new SimpleAdjacencyStorage<Integer, Integer>(true),
                new BreadthFirstSearchTraverse<>(),
                false);
    }
}