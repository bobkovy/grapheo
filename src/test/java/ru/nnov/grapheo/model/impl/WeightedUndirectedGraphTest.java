package ru.nnov.grapheo.model.impl;

import org.junit.Before;
import org.junit.Test;
import ru.nnov.grapheo.GraphBuilder;
import ru.nnov.grapheo.algorithms.pathfinder.impl.Dijkstra;
import ru.nnov.grapheo.model.Graph;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toSet;
import static org.junit.Assert.*;

public class WeightedUndirectedGraphTest {
    private Graph<Integer, Integer> graph;

    @Before
    public void setUp() {
        /*
         *   (8)        (7)
         *               |1
         *           9   |    6
         *   (6) -------(5)------- (4)
         *    | \                 / |
         *    | 2\              / 11|
         * 14 |   \-----(3)------   |  15
         *    | 9 /              \10|
         *    | /        7        \ |
         *   (1) ------------------(2)
         */
        GraphBuilder<Integer, Integer> builder = GraphBuilder.<Integer, Integer>builder()
                .withPathfinder(new Dijkstra<>())
                .withWeightOnEdge(true)
                .withDirection(false);
        IntStream.range(1, 9).forEach(builder::addVertex);
        builder.addEdge(1, 2, 7)
                .addEdge(1, 3, 9)
                .addEdge(1, 6, 14)
                .addEdge(2, 3, 10)
                .addEdge(2, 4, 15)
                .addEdge(3, 4, 11)
                .addEdge(3, 6, 2)
                .addEdge(4, 5, 6)
                .addEdge(5, 6, 9)
                .addEdge(5, 7, 1);
        graph = builder.build();
    }

    @Test(expected = IllegalStateException.class)
    public void noPathFound() {
        graph.getPath(1, 8);
    }

    @Test
    public void findPathWithCycle() {
        assertFalse(graph.isDirected());
        assertEquals(IntStream.range(1, 9).boxed().collect(toSet()), graph.getVertexSet());
        List<Integer> path = graph.getPath(1, 5);
        assertNotNull(path);
        assertEquals(Arrays.asList(1, 3, 6, 5), path);
    }

    @Test
    public void findPathEndVertex() {
        assertFalse(graph.isDirected());
        assertEquals(IntStream.range(1, 9).boxed().collect(toSet()), graph.getVertexSet());
        List<Integer> path = graph.getPath(1, 7);
        assertNotNull(path);
        assertEquals(Arrays.asList(1, 3, 6, 5, 7), path);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeWeightNotSupported() {
        graph = GraphBuilder.<Integer, Integer>builder()
                .withPathfinder(new Dijkstra<>())
                .withWeightOnEdge(true)
                .withDirection(false)
                .addVertex(1)
                .addVertex(2)
                .addVertex(3)
                .addEdge(1, 2, -7)
                .addEdge(1, 3, -9)
                .build();
        graph.getPath(1, 2);
    }

}