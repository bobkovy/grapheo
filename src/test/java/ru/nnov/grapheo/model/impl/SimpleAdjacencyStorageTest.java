package ru.nnov.grapheo.model.impl;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import ru.nnov.grapheo.model.AdjacencyStorage;

import java.util.stream.IntStream;

import static java.util.stream.Collectors.toSet;
import static org.junit.Assert.*;

public class SimpleAdjacencyStorageTest {


    @Test(expected = IllegalArgumentException.class)
    public void addEdgeWithoutWeightToWeightedGraph() {
        AdjacencyStorage<Integer, Integer> adjacencyStorage = new SimpleAdjacencyStorage<>(true);
        assertTrue(adjacencyStorage.isWeighted());
        adjacencyStorage.addToAdjacencyMap(1, 1);
    }

    @Test
    public void addEdgeWithoutWeightToUnweightedGraph() {
        AdjacencyStorage<Integer, Integer> adjacencyStorage = new SimpleAdjacencyStorage<>(false);
        assertFalse(adjacencyStorage.isWeighted());
        adjacencyStorage.addToAdjacencyMap(1, 2);
        adjacencyStorage.addToAdjacencyMap(1, 3);
        assertEquals(IntStream.range(2, 4).boxed().collect(toSet()), adjacencyStorage.getAdjacencyMap().get(1));
        assertEquals(IntStream.range(2, 4).boxed().map(integer -> new ImmutablePair<>(integer, null)).collect(toSet()),
                adjacencyStorage.getAdjacencyMapWithWeight().get(1));
    }
}