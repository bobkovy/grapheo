package ru.nnov.grapheo.model.impl;

import org.junit.Before;
import org.junit.Test;
import ru.nnov.grapheo.GraphBuilder;
import ru.nnov.grapheo.algorithms.pathfinder.impl.BreadthFirstSearch;
import ru.nnov.grapheo.model.Graph;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toSet;
import static org.junit.Assert.*;

public class UnweightedUndirectedGraphTest {
    private Graph<Integer, Integer> graph;

    @Before
    public void setUp() {
        /*
         *     (11)
         *   /
         * (0)-(1)-(2)-(3)-(4)-(5)-(6)-(7)-(8)-(9) (10)
         *  \__________________________________/
         */
        GraphBuilder<Integer, Integer> builder = GraphBuilder.<Integer, Integer>builder()
                .withPathfinder(new BreadthFirstSearch<>())
                .withWeightOnEdge(false)
                .withDirection(false);
        IntStream.range(0, 12).forEach(builder::addVertex);
        IntStream.range(0, 9).forEach(i -> builder.addEdge(i, i + 1));
        graph = builder
                .addEdge(0, 9)
                .addEdge(0, 11)
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void noPathFound() {
        graph.getPath(0, 10);
    }

    @Test
    public void findPathWithCycle() {
        assertFalse(graph.isDirected());
        assertEquals(IntStream.range(0, 12).boxed().collect(toSet()), graph.getVertexSet());
        List<Integer> path = graph.getPath(6, 3);
        assertNotNull(path);
        assertEquals(Arrays.asList(6, 5, 4, 3), path);
    }

    @Test
    public void findPathEndVertex() {
        assertFalse(graph.isDirected());
        assertEquals(IntStream.range(0, 12).boxed().collect(toSet()), graph.getVertexSet());
        List<Integer> path = graph.getPath(3, 11);
        assertNotNull(path);
        assertEquals(Arrays.asList(3, 2, 1, 0, 11), path);
    }

    @Test
    public void addLoop() {
        graph = GraphBuilder.<Integer, Integer>builder()
                .withPathfinder(new BreadthFirstSearch<>())
                .withWeightOnEdge(false)
                .withDirection(false)
                .addVertex(1)
                .addEdge(1, 1)
                .build();
        assertTrue(graph.getAdjacencyMap().isEmpty());
    }

}