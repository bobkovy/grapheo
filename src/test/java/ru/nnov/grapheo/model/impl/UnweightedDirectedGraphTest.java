package ru.nnov.grapheo.model.impl;

import org.junit.Before;
import org.junit.Test;
import ru.nnov.grapheo.GraphBuilder;
import ru.nnov.grapheo.algorithms.pathfinder.impl.BreadthFirstSearch;
import ru.nnov.grapheo.model.Graph;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toSet;
import static org.junit.Assert.*;

public class UnweightedDirectedGraphTest {
    private Graph<Integer, Integer> graph;

    @Before
    public void setUp() {
        /*
         * (12)<-(11) <------------
         *       ↙                 ↖
         *     (0)->(1)->(2)->(3)->(4)<-(5)<-(6)<-(7)<-(8)<-(9) (10)
         *      ↘__________________________________________↗
         */

        GraphBuilder<Integer, Integer> builder = GraphBuilder.<Integer, Integer>builder()
                .withPathfinder(new BreadthFirstSearch<>())
                .withWeightOnEdge(false)
                .withDirection(true);
        IntStream.range(0, 13).forEach(builder::addVertex);
        IntStream.range(0, 4).forEach(i -> builder.addEdge(i, i + 1));
        IntStream.range(4, 10).forEach(i -> builder.addEdge(i + 1, i));
        graph = builder
                .addEdge(0, 9)
                .addEdge(11, 0)
                .addEdge(11, 12)
                .addEdge(4, 11)
                .build();
    }

    @Test(expected = IllegalStateException.class)
    public void noPathFound() {
        graph.getPath(12, 5);
    }

    @Test
    public void findPathWithCycle() {
        assertTrue(graph.isDirected());
        assertEquals(IntStream.range(0, 13).boxed().collect(toSet()), graph.getVertexSet());
        List<Integer> path = graph.getPath(2, 11);
        assertNotNull(path);
        assertEquals(Arrays.asList(2, 3, 4, 11), path);
    }

    @Test
    public void findPathEndVertex() {
        assertTrue(graph.isDirected());
        assertEquals(IntStream.range(0, 13).boxed().collect(toSet()), graph.getVertexSet());
        List<Integer> path = graph.getPath(0, 12);
        assertNotNull(path);
        assertEquals(Arrays.asList(0, 1, 2, 3, 4, 11, 12), path);
    }

    @Test
    public void addLoop() {
        graph = GraphBuilder.<Integer, Integer>builder()
                .withPathfinder(new BreadthFirstSearch<>())
                .withWeightOnEdge(false)
                .withDirection(true)
                .addVertex(1)
                .addEdge(1, 1)
                .build();
        assertTrue(graph.getAdjacencyMap().isEmpty());
    }

}