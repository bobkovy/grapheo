package ru.nnov.grapheo.algorithms.traverse.impl;

import org.junit.Before;
import org.junit.Test;
import ru.nnov.grapheo.GraphBuilder;
import ru.nnov.grapheo.model.Graph;

import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

public class BreadthFirstSearchTraverseTest {

    private Graph<Integer, Integer> graph;

    @Before
    public void setUp() {
        /*
         * (12)<-(11) <------------
         *       ↙                 ↖
         *     (0)->(1)->(2)->(3)->(4)<-(5)<-(6)<-(7)<-(8)<-(9) (10)
         *      ↘__________________________________________↗
         */
        GraphBuilder<Integer, Integer> builder = GraphBuilder.<Integer, Integer>builder()
                .withTraverser(new BreadthFirstSearchTraverse<>())
                .withWeightOnEdge(false)
                .withDirection(true);
        IntStream.range(0, 13).forEach(builder::addVertex);
        IntStream.range(0, 4).forEach(i -> builder.addEdge(i, i + 1));
        IntStream.range(4, 10).forEach(i -> builder.addEdge(i + 1, i));
        graph = builder
                .addEdge(0, 9)
                .addEdge(11, 0)
                .addEdge(11, 12)
                .addEdge(4, 11)
                .build();
    }

    @Test
    public void traverse() {
        final int[] count = {0};
        graph.traverse(integer -> count[0]++);
        assertEquals(graph.getVertexSet().size(), count[0]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void traverseInvalidArgument() {
        final int[] count = {0};
        graph.traverse(null);
        assertEquals(graph.getVertexSet().size(), count[0]);
    }
}